<?php
function generarVectorAleatorio() {
    $vector = array();
    for ($i = 0; $i < 101; $i++) {
        $valor = rand(1, 101);
        array_push($vector, $valor);
    }
    return $vector;
}

$vectorAleatorio = generarVectorAleatorio();

$suma = array_sum($vectorAleatorio);

echo "El vector generado es: " . implode(", ", $vectorAleatorio) . "<br>";
echo "La sumatoria de los elementos es: " . $suma;
?>
