<?php
function buscarCadenaEnArray($cadena, $array) {
    return in_array($cadena, $array);
}

$cadenas = ["Inge", "Visual Studio", "PHP", "Script", "Vector", "Array", "Funcion", "Cadena", "Variable", "Buscar"];

$cadenaABuscar = "Array";

if (buscarCadenaEnArray($cadenaABuscar, $cadenas)) {
    echo "$cadenaABuscar ya existe en el array.";
} else {
    echo "$cadenaABuscar no existe. Se agregará al final del array.<br>";
    
    $cadenas[] = $cadenaABuscar;

    echo "Array actualizado: " . implode(", ", $cadenas);
}
?>
