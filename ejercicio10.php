<?php
function generarVectorAleatorio() {
    $vector = array();
    for ($i = 0; $i < 51; $i++) {
        $valor = rand(1, 1001);
        array_push($vector, $valor);
    }
    return $vector;
}

function encontrarMayor($vector) {
    $mayor = $vector[0];
    $indice = 0;
    
    for ($i = 1; $i < count($vector); $i++) {
        if ($vector[$i] > $mayor) {
            $mayor = $vector[$i];
            $indice = $i;
        }
    }
    
    return array('valor' => $mayor, 'indice' => $indice);
}

$vectorAleatorio = generarVectorAleatorio();

$resultado = encontrarMayor($vectorAleatorio);

echo "El vector generado es: " . implode(", ", $vectorAleatorio) . "<br>";
echo "El valor " . $resultado['valor'] . " posee el mayor valor y se encuentra en el indice " . $resultado['indice'] . ".";
?>
