<?php
function getVisitsCount() {
    $counterFile = 'contador.txt';
    
    if (file_exists($counterFile)) {
        $count = file_get_contents($counterFile);
        return intval($count);
    } else {
        return 0;
    }
}

function incrementVisitsCount() {
    $counterFile = 'contador.txt';
    
    if (file_exists($counterFile)) {
        $count = file_get_contents($counterFile);
        $count = intval($count);
        $count++;
    } else {
        $count = 1;
    }
    
    file_put_contents($counterFile, $count);
}
?>
