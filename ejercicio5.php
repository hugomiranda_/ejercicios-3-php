<?php
// Verifica si se ha enviado el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Recupera los datos del formulario
    $nombre = $_POST['nombre'];
    $apellido = $_POST['apellido'];
    $edad = $_POST['edad'];

    // Imprime los datos introducidos por el usuario
    echo "Nombre: $nombre<br>";
    echo "Apellido: $apellido<br>";
    echo "Edad: $edad años<br>";
} else {
    // Si no se ha enviado el formulario, muestra el formulario
    echo <<<HTML
    <!DOCTYPE html>
    <html>
    <head>
        <title>Ejercicio 5</title>
    </head>
    <body>
        <h1>Formulario PHP</h1>
        <form method="POST" action="">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" id="nombre" required><br><br>

            <label for="apellido">Apellido:</label>
            <input type="text" name="apellido" id="apellido" required><br><br>

            <label for="edad">Edad:</label>
            <input type="number" name="edad" id="edad" required><br><br>

            <input type="submit" value="Enviar">
        </form>
    </body>
    </html>
HTML;
}
?>
