<?php

include 'functions/randomKey.php';
include 'functions/randomValue.php';
include 'functions/randomArray.php';
include 'functions/printFullArray.php';
include 'functions/filterPrint.php';

// Generamos el Array pasando el tamaño que nos gustaria que tenga
$associativeArray = generateAssociativeArray(10);

echo "Vector generado aleatoriamentes:<br>";
printAssociativeArray($associativeArray);

echo "<br>Claves que comienzan con 'a', 'd', 'm' o 'z':<br>";
filterAndPrintKeys($associativeArray);
?>
