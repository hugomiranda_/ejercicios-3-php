<?php

// Imprime solo los que comiencen con ciertas letras
function filterAndPrintKeys($array) {
    $found = false;
    foreach ($array as $key => $value) {
        $firstLetter = substr($key, 0, 1);
        if (in_array($firstLetter, ['a', 'd', 'm', 'z'])) {
            echo "Clave que comienza con '$firstLetter': $key<br>";
            $found = true;
        }
    }

    if (!$found) {
        echo "No se encontraron claves que cumplan con los criterios.<br>";
    }
}

?>