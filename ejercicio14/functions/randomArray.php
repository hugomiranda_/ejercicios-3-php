<?php

// Utilizamos las dos funciones anteriores para colocar el key el value de forma random
function generateAssociativeArray($size) {
    $array = array();
    for ($i = 0; $i < $size; $i++) {
        $key = generateRandomString(rand(5, 10));
        $value = generateRandomValue();
        $array[$key] = $value;
    }
    return $array;
}


?>