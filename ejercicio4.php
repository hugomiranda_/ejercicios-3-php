<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 4</title>
</head>
<body>
<?php
// Se genera los tres números aleatorios entre un de 50 a 900
mt_srand(time());
$num1 = mt_rand(50, 900);
$num2 = mt_rand(50, 900);
$num3 = mt_rand(50, 900);

// Se ordenan los números de mayor a menor
$numeros = [$num1, $num2, $num3];
rsort($numeros);

// Se imprimen los números con colores correspondientes
echo '<p>';
foreach ($numeros as $num) {
    if ($num === max($numeros)) {
        echo '<span style="color: green;">' . $num . '</span> ';
    } elseif ($num === min($numeros)) {
        echo '<span style="color: red;">' . $num . '</span> ';
    } else {
        echo $num . ' ';
    }
}
echo '</p>';
?>
</body>
</html>
