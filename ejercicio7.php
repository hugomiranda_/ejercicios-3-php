<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 7</title>
</head>
<body>
    <h1>Verificador de Palíndromos</h1>
    
    <?php
    function esPalindromo($cadena) {

        $cadena = strtolower(preg_replace('/\s+/', '', $cadena));
        
        $cadenaInvertida = strrev($cadena);
        
        return $cadena == $cadenaInvertida;
    }

    if (isset($_POST['cadena'])) {
        $cadena = $_POST['cadena'];
        if (esPalindromo($cadena)) {
            echo "<p> $cadena es un palíndromo.</p>";
        } else {
            echo "<p>'$cadena' no es un palíndromo.</p>";
        }
    }
    ?>

    <form method="post" action="">
        <label for="cadena">Ingrese una palabra:</label>
        <input type="text" id="cadena" name="cadena" required>
        <input type="submit" value="Verificar">
    </form>
</body>
</html>