<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 8</title>
</head>
<body>
    <h1>Generador de Matriz Aleatoria</h1>
    
    <?php
    function generarMatrizAleatoria($filas, $columnas) {
        echo "<p>Matriz $filas x $columnas:</p>";
        echo "<table border='1'>";
        
        for ($i = 0; $i < $filas; $i++) {
            echo "<tr>";
            for ($j = 0; $j < $columnas; $j++) {
                $numeroAleatorio = rand(1, 100);
                echo "<td>$numeroAleatorio</td>";
            }
            echo "</tr>";
        }
        
        echo "</table><br>";
    }

    if (isset($_POST['filas']) && isset($_POST['columnas'])) {
        $filas = $_POST['filas'];
        $columnas = $_POST['columnas'];
        generarMatrizAleatoria($filas, $columnas);
    }
    ?>

    <form method="post" action="">
        <label for="filas">Número de filas:</label>
        <br>
        <input type="number" id="filas" name="filas" required>
        <br>
        <label for="columnas">Número de columnas:</label>
        <br>
        <input type="number" id="columnas" name="columnas" required>
        <br>
        <input type="submit" value="Generar Matriz">
    </form>
</body>
</html>
