<?php
// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Recupera las credenciales ingresadas por el usuario
    $usuario = $_POST["usuario"];
    $contrasena = $_POST["contrasena"];

    // Ruta al archivo de datos (usuarios.txt)
    $archivo = "usuarios.txt";

    // Verifica si el archivo existe
    if (file_exists($archivo)) {
        // Lee el contenido del archivo
        $lineas = file($archivo, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        // Recorre las líneas en busca de coincidencias de usuario y contraseña
        foreach ($lineas as $linea) {
            list($usuarioGuardado, $contrasenaGuardada) = explode(":", $linea);

            if ($usuario == $usuarioGuardado && $contrasena == $contrasenaGuardada) {
                // Credenciales válidas, redirige a la página de bienvenida
                header("Location: menu.html");
                exit();
            }
        }
    }

    // Si no se encontraron credenciales válidas, muestra un mensaje de error
    echo "Error: Usuario o contraseña incorrectos.";
}
?>
