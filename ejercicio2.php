<?php
echo "Versión de PHP: " . phpversion() . "<br>";

echo "ID de la versión de PHP: " . PHP_VERSION_ID . "<br>";

echo "Valor máximo soportado para enteros: " . PHP_INT_MAX . "<br>";

echo "Tamaño máximo del nombre de un archivo: " . PHP_MAXPATHLEN . "<br>";

echo "Versión del Sistema Operativo: " . php_uname('v') . "<br>";

echo "Símbolo de Fin de Línea" . "<br>";

echo "Include Path por defecto: " . get_include_path() . "<br>";
?>
