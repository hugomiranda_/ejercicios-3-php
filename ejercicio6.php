<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 6</title>
</head>
<body>
    <h1>Calculadora PHP</h1>
    <form method="post" action="">
        <label for="operando1">Operando 1:</label>
        <input type="text" name="operando1" required><br><br>

        <label for="operando2">Operando 2:</label>
        <input type="text" name="operando2" required><br><br>

        <label for="operacion">Operación:</label>
        <select name="operacion">
            <option value="suma">Suma</option>
            <option value="resta">Resta</option>
            <option value="multiplicacion">Multiplicación</option>
            <option value="division">División</option>
        </select><br><br>

        <input type="submit" name="calcular" value="Calcular">
    </form>

    <?php
    if (isset($_POST['calcular'])) {
        $operando1 = $_POST['operando1'];
        $operando2 = $_POST['operando2'];
        $operacion = $_POST['operacion'];

        // Validar que los operandos sean números
        if (!is_numeric($operando1) || !is_numeric($operando2)) {
            echo "Error. Ingresa números válidos como operandos.";
        } else {
            switch ($operacion) {
                case 'suma':
                    $resultado = $operando1 + $operando2;
                    break;
                case 'resta':
                    $resultado = $operando1 - $operando2;
                    break;
                case 'multiplicacion':
                    $resultado = $operando1 * $operando2;
                    break;
                case 'division':
                    if ($operando2 == 0) {
                        echo "No es posible dividir por cero.";
                    } else {
                        $resultado = $operando1 / $operando2;
                    }
                    break;
                default:
                    echo "Operación no válida.";
                    break;
            }

            if (isset($resultado)) {
                echo "Resultado: $resultado";
            }
        }
    }
    ?>
</body>
</html>
