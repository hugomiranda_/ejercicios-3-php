<?php
function preOrderTraversal($arr, $index, $size) {
    if ($index < $size) {
        echo $arr[$index] . " "; // Imprimir el nodo actual
        preOrderTraversal($arr, 2 * $index + 1, $size); // Recorrer el subárbol izquierdo
        preOrderTraversal($arr, 2 * $index + 2, $size); // Recorrer el subárbol derecho
    }
}

$arbolBinario = array(3, 6, 4, 14, 9, 900, 45, 100, 30, 40, 15);
$size = count($arbolBinario);

echo "Recorrido en preorden del árbol binario: ";
preOrderTraversal($arbolBinario, 0, $size);
?>
