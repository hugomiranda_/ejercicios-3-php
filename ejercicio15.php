<?php

function generateRandomValue() {
    return rand(1, 10);
}

// Llena el vector con valores aleatorios
function fillArray($size) {
    $array = [];
    for ($i = 0; $i < $size; $i++) {
        $array[] = generateRandomValue();
    }
    return $array;
}

// Función recursiva para imprimir el vector desde atrás hacia adelante
function printArrayBackward($array, $index) {
    if ($index >= 0) {
        echo $array[$index] . " ";
        printArrayBackward($array, $index - 1);
    }
}

// Crea un vector de 20 elementos
$vector = fillArray(20);

// Llama a la función recursiva para imprimir el vector desde atrás hacia adelante
echo "Vector desde atrás hacia adelante: ";
printArrayBackward($vector, count($vector) - 1);

echo "<br>Vector normal $";
?>
