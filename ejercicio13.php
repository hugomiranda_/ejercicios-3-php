<?php

$vector = [
    "clave1" => "elemento1",
    "clave2" => "elemento2",
    "clave3" => "elemento3",
    "clave4" => "elemento4",
    "clave5" => "elemento5",
    "clave6" => "elemento6",
    "clave7" => "elemento7",
    "clave8" => "elemento8",
    "clave9" => "elemento9",
    "clave10" => "elemento10",
];

$vectorBuscar = [
    "clave11" => "elemento11",
];


$clave = key($vectorBuscar);
$valor = reset($vectorBuscar);


if (array_key_exists($clave, $vector)) {
    
    echo "La clave '$clave' del array de un elemento existe en el array.<br>";

} else {

    echo "La clave '$clave' del array de un elemento no existe en el array.<br>";
    
    if (in_array($valor, $vector)) {
        echo "El valor '$valor' del array de un elemento existe en el array.<br>";

    } else {
        echo "El valor '$valor' del array de un elemento no existe en el array.<br>";
        
        $vector[$clave] = $valor;
        echo "La clave '$clave' y el valor '$valor' se han agregado al array.<br>";
    }
}

echo "Array: " . implode(", ", $vector);
?>
