<?php
$logAcceso = [
    "https://www.facebook.com",
    "https://www.facebook.com",
    "https://www.facebook.com",
    "https://web.whatsapp.com",
    "https://web.whatsapp.com",
    "https://www.instagram.com",
    "https://www.twitch.com",
    "https://www.twitch.com"
];

$conteoAcceso = array();

foreach ($logAcceso as $url) {
    if (array_key_exists($url, $conteoAcceso)) {
        $conteoAcceso[$url]++;
    } else {
        $conteoAcceso[$url] = 1;
    }
}

foreach ($conteoAcceso as $url => $conteo) {
    echo "El usuario accedió a la URL $url $conteo veces.<br>";
}
?>
