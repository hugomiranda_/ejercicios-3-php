<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultados del Formulario</title>
</head>
<body>
    <h1>Resultados del Formulario</h1>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        echo "<p><strong>Campo de Texto:</strong> " . $_POST["nombre"] . "</p>";
        echo "<p><strong>Campo Password:</strong> " . $_POST["contrasena"] . "</p>";

        echo "<p><strong>Select Simple:</strong> " . $_POST["opcion"] . "</p>";

        echo "<p><strong>Select Múltiple:</strong> ";
        if(isset($_POST["opciones_multiple"])){
            foreach ($_POST["opciones_multiple"] as $opcion) {
                echo $opcion . " ";
            }
            echo "</p>";
        } else {
            echo "Ninguna opción seleccionada</p>";
        }

        echo "<p><strong>Checkbox:</strong> ";
        if(isset($_POST["checkbox"])){
            echo "Seleccionado";
        } else {
            echo "No seleccionado";
        }
        echo "</p>";

        echo "<p><strong>Opción Radio:</strong> " . $_POST["radio"] . "</p>";

        echo "<p><strong>Text Area:</strong> " . $_POST["comentarios"] . "</p>";
    } else {
        echo "<p>No se recibieron datos del formulario.</p>";
    }
    ?>

</body>
</html>
