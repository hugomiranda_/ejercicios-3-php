<?php
function buscarNombreApellido($nombre, $apellido) {
    // Abre el archivo "agenda.txt" en modo lectura
    $archivo = fopen('agenda.txt', 'r');

    if ($archivo === false) {
        die('No se pudo abrir el archivo.');
    }

    $encontrado = false;

    // Lee línea por línea el archivo y busca el nombre y apellido
    while (($linea = fgets($archivo)) !== false) {
        // Divide la línea en nombre y apellido usando el espacio como separador
        list($nombreEnArchivo, $apellidoEnArchivo) = explode(' ', $linea);

        // Elimina espacios en blanco al principio y al final de las cadenas
        $nombreEnArchivo = trim($nombreEnArchivo);
        $apellidoEnArchivo = trim($apellidoEnArchivo);

        // Compara el nombre y el apellido con los proporcionados por el usuario
        if (strcasecmp($nombreEnArchivo, $nombre) === 0 && strcasecmp($apellidoEnArchivo, $apellido) === 0) {
            $encontrado = true;
            break;
        }
    }

    // Cierra el archivo
    fclose($archivo);

    // Imprime un mensaje según si se encontró o no el nombre y apellido
    if ($encontrado) {
        echo "Se encontró el nombre $nombre $apellido en el archivo.";
    } else {
        echo "No se encontró el nombre $nombre $apellido en el archivo.";
    }
}

// Verifica si se enviaron datos desde un formulario
if (isset($_POST['nombre']) && isset($_POST['apellido'])) {
    $nombreUsuario = $_POST['nombre'];
    $apellidoUsuario = $_POST['apellido'];

    buscarNombreApellido($nombreUsuario, $apellidoUsuario);
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Buscar Nombre y Apellido</title>
</head>
<body>
    <h1>Buscar Nombre y Apellido</h1>
    <form method="post">
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" required><br><br>
        <label for="apellido">Apellido:</label>
        <input type="text" name="apellido" required><br><br>
        <input type="submit" value="Buscar">
    </form>
</body>
</html>
