<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
    
    define("N", 20);

    echo "<h1>Tabla de Números Pares entre 1 y " . N . "</h1>";
    echo "<table border='1'>";
    echo "<tr><th>Número</th></tr>";

    for ($i = 2; $i <= N; $i += 2) {
        echo "<tr><td>$i</td></tr>";
    }

    echo "</table>";
    ?>

</body>
</html>
